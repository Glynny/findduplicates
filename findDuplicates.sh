#!bin/sh

dump="$HOME/duplicates";

mkdir $dump;
gfind -not -empty -type f -printf "%s\n" > $dump/filesizes.txt;
sort -rn $dump/filesizes.txt > $dump/filesizes-descending.txt;
uniq -d $dump/filesizes-descending.txt > $dump/filesizes-descending-duplicatesizes.txt;

mkdir $dump/filesofsize;

while read filesize; do

	mkdir $dump/filesofsize/$filesize
	gfind -type f -size "${filesize}c" > $dump/filesofsize/"${filesize}"/files.txt;
	
	while read file; do
		md5 -r "$file" >> $dump/filesofsize/"${filesize}"/md5s.txt;
	done < $dump/filesofsize/"${filesize}"/files.txt;
	
	sort $dump/filesofsize/"${filesize}"/md5s.txt > $dump/filesofsize/"${filesize}"/md5s-sorted.txt;
	guniq -w32 --all-repeated=separate $dump/filesofsize/"${filesize}"/md5s-sorted.txt > $dump/filesofsize/"${filesize}"/md5s-sorted-uniq.txt;

	if [ -s "$dump/filesofsize/"${filesize}"/md5s-sorted-uniq.txt" ]; then
		printf "\nDuplicates: of size %d.%06dMB\n" $(expr $filesize / 1000000) $(expr $filesize % 1000000);
		
		while read line; do
			printf "\t%s\n" "${line:33}";
		done < $dump/filesofsize/"${filesize}"/md5s-sorted-uniq.txt;

	fi;

done < $dump/filesizes-descending-duplicatesizes.txt;